### World Architects Assets

The convention is to put libs in a folder named after the lib and a sub folder with the version number of the lib.

```
/<lib>/<version>
```

# AngularJS

Download instructions can be found here. https://docs.angularjs.org/misc/downloading

> If you navigate to http://code.angularjs.org/, you'll see a directory listing with all of the angular versions since we started releasing versioned build artifacts (quite late in the project lifetime). Each directory contains all artifacts that we released for a particular version. Download the version you want and have fun.

AngularJS includes the following packages as well, they're shipped with the main lib an officially supported:

* loader
* resource
* mocks
* animate
* scenario
* touch
* route
* sanitize

## List of Angular Modules

* [UI Router](https://github.com/angular-ui/ui-router)
* [UI Bootstrap](http://angular-ui.github.io/bootstrap/)
* [Restangular](https://github.com/mgonto/restangular)
* [Angular Translate](http://angular-translate.github.io/) [Download](https://github.com/angular-translate/bower-angular-translate/releases)
* [Angular Xeditbale](http://vitalets.github.io/angular-xeditable/)
* [NG I18next](https://github.com/i18next/ng-i18next) I18next Angular wrapper for [I18next](http://i18next.com/)
* [UI Select](https://github.com/angular-ui/ui-select) Native Select2 and Selectize for Angular
* [Angular File Upload](https://github.com/danialfarid/angular-file-upload)
* [Angular Tags](https://github.com/mbenford/ngTagsInput) [Download](https://github.com/mbenford/ngTagsInput/releases)
* [Angular Underscore](https://github.com/floydsoft/angular-underscore)
* [Angular Toggle Switch](https://github.com/cgarvis/angular-toggle-switch/releases)
* [Angular Moment](https://github.com/urish/angular-moment)
* [Angular Bootstrap Datetimepicker](https://github.com/dalelotts/angular-bootstrap-datetimepicker)

# Jquery UI

Has been downloaded from here. http://jqueryui.com/download/#!themeParams=none

Every module is included but no theme is included.

##List of Jquery Modules

* [Jquery UI](http://jqueryui.com/)
* [FancyBox Pack 2](https://github.com/fancyapps/fancyBox/) [Download](https://github.com/fancyapps/fancyBox/releases)
* [TagCloud](https://github.com/addywaddy/jquery.tagcloud.js)
* [DateTimePicker](https://github.com/smalot/bootstrap-datetimepicker)
* [DatePicker](https://github.com/eternicode/bootstrap-datepicker)
* [Chosen](https://github.com/harvesthq/chosen/releases)
* [Jquery Actual](https://github.com/dreamerslab/jquery.actual)
* [x-editable](http://github.com/vitalets/x-editable)
* [Jeditable](https://github.com/tuupola/jquery_jeditable)

# List of other libraries

* [Lodash](https://lodash.com/)
* [Underscore](http://underscorejs.org)
* [Modernizer](http://modernizr.com/download/) [Build](http://modernizr.com/download/#-fontface-backgroundsize-borderimage-borderradius-boxshadow-flexbox-hsla-multiplebgs-opacity-rgba-textshadow-cssanimations-csscolumns-generatedcontent-cssgradients-cssreflections-csstransforms-csstransforms3d-csstransitions-applicationcache-canvas-canvastext-draganddrop-hashchange-history-audio-video-indexeddb-input-inputtypes-localstorage-postmessage-sessionstorage-websockets-websqldatabase-webworkers-geolocation-inlinesvg-smil-svg-svgclippaths-touch-webgl-shiv-cssclasses-addtest-prefixed-teststyles-testprop-testallprops-hasevent-prefixes-domprefixes-load)
* [Messageformat.js](https://github.com/SlexAxton/messageformat.js) Required by angular-translate. It doesn't come with a minified file so you need to do it.
* [I18next](http://i18next.com/) Generic JS Translation Lib
* [MomentJS](http://momentjs.com/)

# List of css libraries

* [Twitter Bootstrap](http://getbootstrap.com/)
* [Flag icons](http://lipis.github.io/flag-icon-css/)

# Special notes, pay attention to them!

* angular-libs/ui-select/0.9.6-custom

Is a build of [this fork](https://github.com/JeffGreat/ui-select/) based on commit id [b8bcd8a5](https://github.com/JeffGreat/ui-select/commit/b8bcd8a5f3d0c92e8bfe680cacd78e449bc584d1). The reason for that is that at the time of this writing [PR #256](https://github.com/angular-ui/ui-select/pull/256) was not merged into the main repository. Remove the custom build and this note as soon as it was merged.
